package bzzbomber.model.utilities;

/**
 * This enumeration is an utilities for the translation on movement e key's
 * read.
 */

public enum Input {
    /**
     * Input.
     */
    UP, DOWN, LEFT, RIGHT, SPACE;

}
